<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Campaigns;

/**
 * CampaignsSearch represents the model behind the search form of `app\models\Campaigns`.
 */
class CampaignsSearch extends Campaigns
{
    public $domain;
    public $traffic_source;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['campaign_name', 'clicks', 'leads', 'conversion_rate', 'domain', 'traffic_source'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaigns::find();
        $query->joinWith('infos', true)->asArray()->select('*');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['domain'] = [
            'asc' => [Campaignsinfo::tableName().'.domain' => SORT_ASC],
            'desc' => [Campaignsinfo::tableName().'.domain' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['traffic_source'] = [
            'asc' => [Campaignsinfo::tableName().'.traffic_source' => SORT_ASC],
            'desc' => [Campaignsinfo::tableName().'.traffic_source' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'campaign_name', $this->campaign_name])
            ->andFilterWhere(['like', 'clicks', $this->clicks])
            ->andFilterWhere(['like', 'leads', $this->leads])
            ->andFilterWhere(['like', 'conversion_rate', $this->conversion_rate])
            ->andFilterWhere(['like', campaignsinfo::tableName().'.domain', $this->domain])
            ->andFilterWhere(['like', campaignsinfo::tableName().'.traffic_source', $this->traffic_source])
            ;

        return $dataProvider;
    }
}
