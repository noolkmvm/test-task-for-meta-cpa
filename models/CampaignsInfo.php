<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campaigns_info".
 *
 * @property int $id
 * @property string|null $domain
 * @property string|null $traffic_source
 * @property int|null $campaign_id
 *
 * @property Campaigns $campaign
 */
class CampaignsInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campaigns_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaign_id'], 'integer'],
            [['domain', 'traffic_source'], 'string', 'max' => 255],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaigns::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'domain' => 'Domain',
            'traffic_source' => 'Traffic Source',
            'campaign_id' => 'Campaign ID',
        ];
    }

    /**
     * Gets query for [[Campaign]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaigns::className(), ['id' => 'campaign_id']);
    }
}
