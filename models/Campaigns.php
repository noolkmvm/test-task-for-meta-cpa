<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campaigns".
 *
 * @property int $id
 * @property string|null $campaign_name
 * @property int|null $clicks
 * @property int|null $leads
 * @property int|null $conversion_rate
 *
 * @property CampaignsInfo[] $campaignsInfos
 */
class Campaigns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campaigns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clicks', 'leads', 'conversion_rate'], 'integer'],
            [['campaign_name'], 'string', 'max' => 255],
            [['campaign_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_name' => 'Campaign Name',
            'clicks' => 'Clicks',
            'leads' => 'Leads',
            'conversion_rate' => 'Conversion Rate',
        ];
    }

    /**
     * Gets query for [[CampaignsInfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInfos()
    {
        return $this->hasMany(CampaignsInfo::className(), ['campaign_id' => 'id']);
    }
}
