<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Campaigns;
use app\models\Campaignsinfo;

class DataController extends Controller
{
    public function actionGet()
    {
        $url = 'http://209.97.133.143/tz/api.php?key=20e62b46af322affca0d9bccb7a';
        $arr = @json_decode(file_get_contents($url), TRUE);
        
        foreach ($arr as $row):
            if(!empty($row['clicks']) && !empty($row['leads'])){
                $conversion_rate = ((intval($row['clicks']) / intval($row['leads'])) * 100);
            }else $conversion_rate = '0';

            $clicks = intval($row['clicks']);
            $leads = intval($row['leads']);

            $campaign = new Campaigns();
            $campaign->campaign_name = $row['name'];
            $campaign->clicks = $clicks;
            $campaign->leads = $leads;
            $campaign->conversion_rate = $conversion_rate;
            $campaign->save();

            $campaigns_info = new CampaignsInfo();
            $campaigns_info->domain = $row['domain_name'];
            $campaigns_info->traffic_source	 = $row['ts_name'];
            $campaigns_info->campaign_id = $campaign->id;
            $campaigns_info->save();
        endforeach;
    }
}
