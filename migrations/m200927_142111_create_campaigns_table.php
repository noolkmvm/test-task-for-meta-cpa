<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%campaigns}}`.
 */
class m200927_142111_create_campaigns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%campaigns}}', [
            'id' => $this->primaryKey(),
            'campaign_name' => $this->string()->unique(),
            'clicks' => $this->integer(),
            'leads' => $this->integer(),
            'conversion_rate' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%campaigns}}');
    }
}
