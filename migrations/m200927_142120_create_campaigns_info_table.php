<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%campaigns_info}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%campaigns}}`
 */
class m200927_142120_create_campaigns_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%campaigns_info}}', [
            'id' => $this->primaryKey(),
            'domain' => $this->string(),
            'traffic_source' => $this->string(),
            'campaign_id' => $this->integer(),
        ]);

        // creates index for column `campaign_id`
        $this->createIndex(
            '{{%idx-campaigns_info-campaign_id}}',
            '{{%campaigns_info}}',
            'campaign_id'
        );

        // add foreign key for table `{{%campaigns}}`
        $this->addForeignKey(
            '{{%fk-campaigns_info-campaign_id}}',
            '{{%campaigns_info}}',
            'campaign_id',
            '{{%campaigns}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%campaigns}}`
        $this->dropForeignKey(
            '{{%fk-campaigns_info-campaign_id}}',
            '{{%campaigns_info}}'
        );

        // drops index for column `campaign_id`
        $this->dropIndex(
            '{{%idx-campaigns_info-campaign_id}}',
            '{{%campaigns_info}}'
        );

        $this->dropTable('{{%campaigns_info}}');
    }
}
